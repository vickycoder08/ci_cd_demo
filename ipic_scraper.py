import datetime
import requests
import pytz
import random
import json

from bson import ObjectId
import threading
import pandas as pd
from settings import *


class Ipic_Scraper:

    def run(self, date_loop, mongo_connect):
        self.circuits_locations = mongo_connect.essentials['circuits_locations']
        self.circuit_scraper = mongo_connect.domestic['circuit_scraper']
        self.movie_mapping = mongo_connect.movie_master['movie_mapping']
        self.fq_movie_master = mongo_connect.domestic['fq_movie_master']
        self.closed_theatre = mongo_connect.movie_master["closed_theatre"]
        self.get_showtime(date_loop, mongo_connect)
    
    def get_showtime(self, date_loop, mongo_connect):
        try:
            for d_date in date_loop:
                print(d_date)
                date_update({'date': d_date}, {"$set": {'ipic_flag': 0}})
                date_str = d_date
                break
                    
            #     with self.circuits_locations.find({"circuit_id": 915}, no_cursor_timeout=True) as locations:
            #         for row in locations:
            #             headers = {
            #                         "apikey": "F303064F3FEA4889908E2EE7788726DD"
            #                         }

            #             response = requests.get(f"https://api.ipic.com/iPicAPI2/movies/{row['theatre_id']}", headers=headers)
            #             for i in response.json():
            #                 movie_response = requests.get(f"https://api.ipic.com/iPicAPI2/sessions/{row['theatre_id']}?showDate={date_str.replace('-', '')}&movieID={i['movieID']}", headers=headers)
            #                 for movie_details in movie_response.json():
            #                     showtime24 = movie_details['startDateTime'].split('T')[1]
            #                     showtime24 = ":".join(showtime24.split(':')[:-1])
            #                     ticketing_url = f"https://www.ipic.com/austin/movies/session/{int(movie_details['sessionID'])}?siteId={row['theatre_id']}"
            #                     ticketing_available = 'available' if movie_details['isSoldOut'] != True else "soldout"
            #                     dateInUTC = self.convertUTC(date_str + " " + showtime24 + ":00", row['theatre_timezone'])

            #                     try:
            #                         mm_movie_id=self.fq_movie_master.find_one({"title":i['name']})['id']
            #                     except:
            #                         try:
            #                             mm_movie_id=self.movie_mapping.find_one({"title":i['name']})['id']
            #                         except:
            #                             mm_movie_id=None
                                
            #                     try:
            #                         movie_title=self.fq_movie_master.find_one({"id":mm_movie_id})['title']
            #                     except:
            #                         if mm_movie_id == None or mm_movie_id != None: movie_title = i['name']
                                
            #                     try:
            #                         release_date=self.fq_movie_master.find_one({"id":mm_movie_id})['release_date']
            #                     except:
            #                         release_date=None
                                
            #                     try:
            #                         studio_name=self.fq_movie_master.find_one({"id":mm_movie_id})['studio_name']
            #                     except:
            #                         studio_name=None
                                                
            #                     showtimeRecords = self.circuit_scraper.find_one({"$and":[
            #                                                                                 {"circuit_id": 915},
            #                                                                                 {"date": str(date_str)},
            #                                                                                 {"time": str(showtime24)},
            #                                                                                 {"ticketing_url": ticketing_url},
            #                                                                                 {"theater_id": int(row['theatre_id'])}
            #                                                                             ]
            #                                                                     })
            #                     # print(showtimeRecords)
            #                     if showtimeRecords != None:
            #                         dataValues = {
            #                                         "id": int(movie_details['sessionID']),
            #                                         "ticketing_url": ticketing_url,
            #                                         "status": 'DONE' if ticketing_available == "soldout" else "PENDING",
            #                                         "localtz": dateInUTC.split(' ')[1],
            #                                         "last_updated": str(datetime.datetime.utcnow()),
            #                                         "local_datetime": dateInUTC,
            #                                         "formats": 'Standard',
            #                                         "movie_format": '2D',
            #                                         "movie_name": movie_title,
            #                                         "mm_movie_id":mm_movie_id,
            #                                         "server":random.randint(1, 20),
            #                                         "ticketing_available": ticketing_available,
            #                                         "ticketing_server": "t_" + str(random.randint(1,5)) if date_str == str(datetime.datetime.now()).split()[0] else None,
            #                                         "running_date": str(datetime.datetime.now()).split()[0],
            #                                         "release_date":release_date,
            #                                         "studio_name":studio_name,
            #                                         "before_status": 'DONE' if ticketing_available == "soldout" else "PENDING",
            #                                         "on_status": 'DONE' if ticketing_available == "soldout" else "PENDING",
            #                                         "after_status": 'DONE' if ticketing_available == "soldout" else "PENDING",
            #                                         "error_fail_state": 0,
            #                                         "to_mysql": 0
            #                                     }
            #                         print("============update===========")
            #                         print(dataValues)
            #                         self.circuit_scraper.update_one( {'_id': ObjectId(str(showtimeRecords['_id']))}, { "$set": dataValues } )
            #                     else:
            #                         dataValues = {
            #                                         "id": int(movie_details['sessionID']),
            #                                         "theater_id": int(row['theatre_id']),
            #                                         "theater_name": row['theatre_name'],
            #                                         "address": row['theatre_address'],
            #                                         "city": row['theatre_city'],
            #                                         "state": row['theatre_state'],
            #                                         "zipcode": row['theatre_zipcode'],
            #                                         "movie_id": i['movieID'],
            #                                         "movie_name": movie_title,
            #                                         "rating": i['rating'],
            #                                         "runtime": str(i['duration']) + " mins",
            #                                         "formats": 'Standard',
            #                                         "amenities": '',
            #                                         "date": date_str,
            #                                         "time": showtime24,
            #                                         "status": 'DONE' if ticketing_available == "soldout" else "PENDING",
            #                                         "localtz": dateInUTC.split(' ')[1],
            #                                         "last_updated": str(datetime.datetime.utcnow()),
            #                                         "local_datetime": dateInUTC,
            #                                         "auditorium": movie_details['auditoriumNumber'],
            #                                         "ticketing_url": ticketing_url,
            #                                         "ticketing_available": ticketing_available,
            #                                         "price": '',
            #                                         "total_seats":None,
            #                                         "available":None,
            #                                         "reserved":None,
            #                                         "checkered":None,
            #                                         "error_msg":'',
            #                                         "fandango_movie_id": None,
            #                                         "circuit_id": 915,
            #                                         "movie_format": '2D',
            #                                         "mm_movie_id":mm_movie_id,
            #                                         "server":random.randint(1, 20),
            #                                         "ticketing_server": "t_" + str(random.randint(1,5)) if date_str == str(datetime.datetime.now()).split()[0] else None,
            #                                         "before_total_seats":None,
            #                                         "before_available":None,
            #                                         "before_reserved":None,
            #                                         "before_checkered":None,
            #                                         "on_total_seats":None,
            #                                         "on_available":None,
            #                                         "on_reserved":None,
            #                                         "on_checkered":None,
            #                                         "after_total_seats":None,
            #                                         "after_available":None,
            #                                         "after_reserved":None,
            #                                         "after_checkered":None,
            #                                         "before_status": 'DONE' if ticketing_available == "soldout" else "PENDING",
            #                                         "on_status": 'DONE' if ticketing_available == "soldout" else "PENDING",
            #                                         "after_status": 'DONE' if ticketing_available == "soldout" else "PENDING",
            #                                         "before_error_msg":'',
            #                                         "on_error_msg":'',
            #                                         "after_error_msg":'',
            #                                         "release_date":release_date,
            #                                         "studio_name":studio_name,
            #                                         "running_date": str(datetime.datetime.now()).split()[0],
            #                                         "error_fail_state": 0,
            #                                         "to_mysql": 0
            #                                     }
            #                         print("-------INSERT--------")
            #                         print(dataValues)
            #                         self.circuit_scraper.insert_one(dataValues)

                # completion_date = { 'date': date_str } 
                # date_flag = { "$set": { 'ipic_flag': 2 } }            
                # self.date_db.update_one(completion_date, date_flag)
        except Exception as e: 
            print(e)
            pass
    
    def convertUTC(self, dateString, timezone):
        local = pytz.timezone(timezone)
        temp = datetime.datetime.strptime(dateString, "%Y-%m-%d %H:%M:%S")
        local_dt = local.localize(temp, is_dst=None)
        utc_dt = local_dt.astimezone(pytz.utc)
        return utc_dt.strftime("%Y-%m-%d %H:%M")


def scraper_main(*d_date):
    Ipic = Ipic_Scraper()
    Ipic.run(d_date[0], d_date[1])

def main():
    mongo_connect = mongo_db()
    len_date = [i['date'] for i in mongo_connect.essentials['dates'].find({"ipic_flag": 0})]
    len_date.sort()

    k, m = divmod(len(len_date), 4)
    t_date = list(len_date[i*k+min(i, m):(i+1)*k+min(i+1, m)] for i in range(4))

    thread1 = threading.Thread(target=scraper_main, args=(t_date[0], mongo_connect))
    # thread2 = threading.Thread(target=scraper_main, args=(t_date[1], mongo_connect))
    # thread3 = threading.Thread(target=scraper_main, args=(t_date[2], mongo_connect))
    # thread4 = threading.Thread(target=scraper_main, args=(t_date[3], mongo_connect))

    thread1.start()
    # thread2.start()
    # thread3.start()
    # thread4.start()

    thread1.join()
    # thread2.join()
    # thread3.join()
    # thread4.join()

    mongo_connect.close()


if __name__ == '__main__':
    main()