from dotenv.main import load_dotenv
from pymongo import MongoClient
import boto3
import os
import json
import time
import random

load_dotenv()

def mongo_db():
    mongo_connect = MongoClient(os.environ['connection_string'])
    return mongo_connect


def aws_connect_s3():
    session = boto3.Session(aws_access_key_id=os.environ['aws_access_key_id'], aws_secret_access_key=os.environ['aws_secret_access_key'], region_name=os.environ['region_name'])
    s3 = session.resource('s3')
    return s3, os.environ['bucket_name']


def date_update(completion_date, date_flag):
    mongo_connect = MongoClient(os.environ['mongodb'])
    mongo_connect.essentials['dates'].update_one(completion_date, date_flag)
    mongo_connect.close()
    # s3, bucket_name = aws_connect_s3()
    # data_string = json.dumps(df, indent=4, default=str)
    # obj = s3.Object(bucket_name, f"essentials/dates/{circuit}.json")
    # obj.put(Body=data_string)
    # print(obj)